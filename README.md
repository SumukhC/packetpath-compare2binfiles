########################README.md###############################

The program takes 2 arguments - the 2 file names (files to be compared).
The program prints out the 16 bytes, starting from the offset where the difference occurs, of the file to stdout.

The program returns a value of 0 if the execution was fine; else a value of -1.

Time taken if computed for compare operation - comparing each byte to see if they differ.