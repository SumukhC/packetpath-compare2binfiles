#include<stdio.h>
#include<time.h>
#include<stdlib.h>

int compare_bin_files(FILE *fp_bin_file1, FILE *fp_bin_file2) {
	//Keep track of the file1 size
	fseek (fp_bin_file1, 0L, SEEK_END);
	long int size_file1 = ftell (fp_bin_file1);
	rewind(fp_bin_file1);
	
	//Keep track of the file2 size
	fseek (fp_bin_file2, 0L, SEEK_END);
	long int size_file2 = ftell (fp_bin_file2);
	rewind (fp_bin_file2);
	
	short int *ptr1 = (short int*) malloc (size_file1 * sizeof (short int));
	short int *ptr2 = (short int*) malloc (size_file2 * sizeof (short int));
	//printf("Sizes: %d %d\n", size_file1, size_file2);
	fread (ptr1, sizeof(unsigned char) * 2, size_file1, fp_bin_file1);
	fread (ptr2, sizeof(unsigned char) * 2, size_file2, fp_bin_file2);
	int i = 0;
	int j = 0;
	
	while (i < size_file1 && j < size_file2) {
		
		if (ptr1[i] ^ ptr2[j]) {
			printf("Files differ...\n");
			printf("\nFile 1: \n");
			fwrite(&ptr1[i], sizeof(short int), 8, stdout); //printing 16 bytes - 2 * 8 short int
			printf("\nFile 2: \n");
			fwrite(&ptr2[j], sizeof(short int), 8, stdout); //printing 16 bytes - 2 * 8 short int
			printf("\n");
			fclose(fp_bin_file1);
			fclose(fp_bin_file2);
			return 0;
		}
		i++;
		j++;
	}
	return 1;
}

int main(int argc, char *argv[]) {
	if (3 != argc) {	//Incorrect number of arguments
		
		printf("Wrong number of arguments. Format: FileName BinaryFile1 BinaryFile2\n");
		printf("You supplied %d arguments.\n", argc);
		return -1;
		
	} else {
		clock_t t;
		FILE *fp_bin_file1;
		FILE *fp_bin_file2;
		
		fp_bin_file1 = fopen(argv[1], "rb");
		fp_bin_file2 = fopen(argv[2], "rb");
		
		if (fp_bin_file1 == NULL) {	//Issues with opening the file
			printf("Error encountered while opening %s.", argv[1]);
			return -1;
		}
		
		if (fp_bin_file2 == NULL) {	//Issues with opening the file
			printf("Error encountered while opening %s.", argv[2]);
			return -1;
		}
		t = clock();
		if (compare_bin_files(fp_bin_file1, fp_bin_file2)) {
			printf("No diff");
		}
		t = clock() - t;
		printf("\nTime to execute: %f ms\n", (((float) t)/CLOCKS_PER_SEC) * 1000) ;
		
		return 0;
	}
}
